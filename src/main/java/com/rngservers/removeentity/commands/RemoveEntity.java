package com.rngservers.removeentity.commands;

import com.rngservers.removeentity.entity.EntityManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class RemoveEntity implements CommandExecutor {
    private EntityManager manager;

    public RemoveEntity(EntityManager manager) {
        this.manager = manager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("removeentity.use")) {
                Entity entity = manager.getEntityFront(player);
                if (entity != null) {
                    if (entity instanceof Player) {
                        player.sendMessage("Can't remove player entities!");
                        return true;
                    }
                    entity.remove();
                    player.sendMessage("Removed entity: " + entity.getType());
                } else {
                    player.sendMessage("No entity found!");
                }
            } else {
                player.sendMessage("No permission!");
            }
        } else {
            sender.sendMessage("This command can only be run by a player!");
        }
        return true;
    }
}
