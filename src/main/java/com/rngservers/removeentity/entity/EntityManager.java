package com.rngservers.removeentity.entity;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class EntityManager {
    private boolean isLookingAt(Player player, Entity entity) {
        Location playerEye = player.getEyeLocation();
        Vector toEye = entity.getLocation().toVector().subtract(playerEye.toVector());
        double dotEye = toEye.normalize().dot(playerEye.getDirection());

        return dotEye > 0.90;
    }

    public Entity getEntityFront(Player player) {
        List<Entity> entities = new ArrayList<>();
        for (Entity entity : player.getNearbyEntities(5, 5, 5)) {
            if (entity instanceof Entity) {
                if (isLookingAt(player, entity)) {
                    entities.add(entity);
                }
            }
        }
        if (entities.isEmpty()) {
            return null;
        } else {
            return entities.get(0);
        }
    }
}
