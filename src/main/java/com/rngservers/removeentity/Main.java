package com.rngservers.removeentity;

import com.rngservers.removeentity.commands.RemoveEntity;
import com.rngservers.removeentity.entity.EntityManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        EntityManager manager = new EntityManager();
        this.getCommand("removeentity").setExecutor(new RemoveEntity(manager));
    }
}
